"""
Trivial project is just for trying out Git and the
grading mechanisms.

This file uses another credentials file to read and print a message
"""

import configparser
config = configparser.ConfigParser()
config.read("credentials.ini")

message = config["DEFAULT"]["message"]

print(message)
